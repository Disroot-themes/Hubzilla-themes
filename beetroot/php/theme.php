<?php

/**
 *   * Name: Beetroot
 *   * Description: Redbasic Derived theme
 *   * Version: 2.1
 *   * MinVersion: 2.3.1
 *   * MaxVersion: 6.0
 *   * Author: Antilopa
 *   * Compat: Red [*]
 *
 */

function beetroot_init(&$a) {

    App::$theme_info['extends'] = 'redbasic';


}
