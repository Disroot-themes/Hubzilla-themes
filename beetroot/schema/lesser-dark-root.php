<?php

	if (! $radius)
		$radius = '0.1rem';
	if (! $shadow)
		$shadow = '0.2rem';
	if (! $bgcolour)
			$bgcolour = '#343a40';
	if (! $font_colour)
			$font_colour = '#a9a9a9';
/*	if (! $nav_bg)
			$nav_bg = '#343a40';*/
	if (! $item_colour)
	    $item_colour = 'rgb(20,20,20)';
	if (! $comment_item_colour)
	    $comment_item_colour = 'rgb(50,50,50)';
	if (! $link_bgcolour)
	    $link_bgcolour = 'rgb(40,40,40)';
