<?php

	if (! $radius)
		$radius = '0.1rem';
	if (! $shadow)
		$shadow = '0.2rem';
	if (! $bgcolour)
			$bgcolour = '#000';
	if (! $font_colour)
			$font_colour = '#a9a9a9';
	if (! $nav_bg)
			$nav_bg = '#000';
	if (! $item_colour)
	    $item_colour = 'rgb(20,20,20)';
	if (! $comment_item_colour)
	    $comment_item_colour = 'rgb(30,30,30)';
	if (! $link_bgcolour)
	    $link_bgcolour = 'rgb(30,30,30)';
